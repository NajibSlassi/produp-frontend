import React from 'react';
import { Rnd } from 'react-rnd';
import { Paper } from '@material-ui/core';;

export default (props) => (
    <Rnd>
        <Paper style={{ margin: 0, height: '100%', width: '100px', backgroundColor: "yellow" }} elevation={3}>
            <div style={{ padding: '10px', height: props.taskDuration * 100 + "px" }}>
                <strong>{props.task.name}</strong>
                <div>{props.task.startDateTime}</div>
                <div>{props.task.endDateTime}</div>
            </div>
        </Paper>
    </Rnd>

);