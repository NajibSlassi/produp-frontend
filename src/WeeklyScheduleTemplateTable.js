import React, { useState } from 'react';
import './WeeklyScheduleTemplate.scss';
import DatePicker from './DatePicker';
import TimePickers from './TimePicker';
import GridCell from './GridCell';
import { useTable } from 'react-table';
import styled from 'styled-components';

const Styles = styled.div`
  padding: 1rem;

  table {
    border-spacing: 0;
    border: 1px solid black;

    tr {
      :last-child {
        td {
          border-bottom: 0;
        }
      }
    }

    th,
    td {
      margin: 0;
      padding: 0.5rem;
      border-bottom: 1px solid black;
      border-right: 1px solid black;

      :last-child {
        border-right: 0;
      }
    }
  }
`

function Table({ columns, data }) {
  // Use the state and functions returned from useTable to build your UI
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
  } = useTable({
    columns,
    data,
  })

  // Render the UI for your table
  return (
    <table {...getTableProps()}>
      <thead>
        {headerGroups.map(headerGroup => (
          <tr {...headerGroup.getHeaderGroupProps()}>
            {headerGroup.headers.map(column => (
              <th {...column.getHeaderProps()}>{column.render('Header')}</th>
            ))}
          </tr>
        ))}
      </thead>
      <tbody {...getTableBodyProps()}>
        {rows.map((row, i) => {
          prepareRow(row)
          return (
            <tr {...row.getRowProps()}>
              {row.cells.map(cell => {
                return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
              })}
            </tr>
          )
        })}
      </tbody>
    </table>
  )
}

function WeeklyScheduleTemplateTable() {

  const [date, setDate] = useState(new Date());
  const [tasks, setTasks] = useState([{ id: 0, name: 'task1', startDateTime: '2020-08-04 08:00:00', endDateTime: '2020-08-04 10:00:00' },
  { id: 1, name: 'task2', startDateTime: '2020-08-05 09:00:00', endDateTime: '2020-08-05 11:00:00' }]);
  const [startingTime, setStartingTime] = useState("08:00");
  const [numberOfDisplayedHoursPerDay, setNumberOfDisplayedHoursPerDay] = useState(16);
  const onChangeDate = (event) => {
    setDate(new Date(event.target.value));
  }
  const onChangeStartingTime = (event) => {
    setStartingTime(event.target.value);
  }

  const hourIncrementer = (timeString, numberOfHours) => {
    const displayedHour = Number(timeString.slice(0, 2)) + numberOfHours;

    return displayedHour - 10 < 0 ? "0" + displayedHour + ":" + timeString.slice(3) : displayedHour + ":" + timeString.slice(3);
  }

  const dateTimeObjectToDateString = (dateTimeObject) => {
    return dateTimeObject.toISOString().slice(0, 10);
  }

  const dateDayIncrementer = (date, numberOfDays) => {
    const intermediateDate = new Date(date)
    return new Date(intermediateDate.setDate(intermediateDate.getDate() + numberOfDays))
  }
  var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

  const checkTimeStringIsValidWhenIncremented = (timeString, incrementingValue) => Number(hourIncrementer(timeString, incrementingValue).slice(0, 2)) < 24
  const firstColumnDay = days[date.getDay()];
  const secondColumnDay = days[(date.getDay() + 1) % 7];
  const thirdColumnDay = days[(date.getDay() + 2) % 7];
  const fourthColumnDay = days[(date.getDay() + 3) % 7];
  const fifthColumnDay = days[(date.getDay() + 4) % 7];
  const sixthColumnDay = days[(date.getDay() + 5) % 7];
  const seventhColumnDay = days[(date.getDay() + 6) % 7];

  const columns = [
    {
      Header: 'Hour',
      accessor: 'hour',
    },
    {
      Header: firstColumnDay,
      accessor: firstColumnDay,
    },
    {
      Header: secondColumnDay,
      accessor: secondColumnDay,
    },
    {
      Header: thirdColumnDay,
      accessor: thirdColumnDay,
    },
    {
      Header: fourthColumnDay,
      accessor: fourthColumnDay,
    },
    {
      Header: fifthColumnDay,
      accessor: fifthColumnDay,
    },
    {
      Header: sixthColumnDay,
      accessor: sixthColumnDay,
    },
    {
      Header: seventhColumnDay,
      accessor: seventhColumnDay,
    }
  ]

  const data = [...Array(numberOfDisplayedHoursPerDay)].filter((x, i) => checkTimeStringIsValidWhenIncremented(startingTime, i)).map((x, i) => {
    return {
      hour: hourIncrementer(startingTime, i),
      [firstColumnDay]:
        <GridCell date={dateTimeObjectToDateString(date)}
          hour={hourIncrementer(startingTime, i)} tasks={tasks}></GridCell>,
      [secondColumnDay]:
        <GridCell date={dateTimeObjectToDateString(dateDayIncrementer(date, 1))}
          hour={hourIncrementer(startingTime, i)} tasks={tasks}></GridCell>,
      [thirdColumnDay]:
        <GridCell date={dateTimeObjectToDateString(dateDayIncrementer(date, 2))}
          hour={hourIncrementer(startingTime, i)} tasks={tasks}></GridCell>,
      [fourthColumnDay]:
        <GridCell date={dateTimeObjectToDateString(dateDayIncrementer(date, 3))}
          hour={hourIncrementer(startingTime, i)} tasks={tasks}></GridCell>,
      [fifthColumnDay]:
        <GridCell date={dateTimeObjectToDateString(dateDayIncrementer(date, 4))}
          hour={hourIncrementer(startingTime, i)} tasks={tasks}></GridCell>,
      [sixthColumnDay]:
        <GridCell date={dateTimeObjectToDateString(dateDayIncrementer(date, 5))}
          hour={hourIncrementer(startingTime, i)} tasks={tasks}></GridCell>,
      [seventhColumnDay]:
        <GridCell date={dateTimeObjectToDateString(dateDayIncrementer(date, 6))}
          hour={hourIncrementer(startingTime, i)} tasks={tasks}></GridCell>

    }
  })



  return (<>
    <DatePicker date={dateTimeObjectToDateString(date)} onChangeDate={onChangeDate} />
    <TimePickers startingTime={startingTime} onChangeStartingTime={onChangeStartingTime} />

    <Styles>
      <Table columns={columns} data={data} />
    </Styles>
  </>
  );
}

export default WeeklyScheduleTemplateTable;
