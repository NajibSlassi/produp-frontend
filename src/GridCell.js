import React from 'react';
import './GridCell.scss';
import DraggableAndResizableComponent from './DraggableAndResizableComponent';

function GridCell(props) {

  const mysqlToJSDate = (mysqlForme) => {
    const dateTime = mysqlForme;

    let dateTimeParts = dateTime.split(/[- :]/); // regular expression split that creates array with: year, month, day, hour, minutes, seconds values
    dateTimeParts[1]--; // monthIndex begins with 0 for January and ends with 11 for December so we need to decrement by one
    const dateObject = new Date(...dateTimeParts); // our Date object
    return dateObject;
  }

  let taskToDisplay = null;

  let taskDuration = null;

  const searchTaskTimingMatchedWithGrid = (tasks, date, hour) => {
    tasks && tasks.map((task) => {
      const startingHour = new Date(date + " " + hour)
      const intermediateObject = new Date(startingHour)
      const endingHour = new Date(intermediateObject.setHours(startingHour.getHours() + 1))

      const taskStartDateTime = mysqlToJSDate(task.startDateTime);

      if (taskStartDateTime >= startingHour && taskStartDateTime < endingHour) {
        taskDuration = (mysqlToJSDate(task.endDateTime) - mysqlToJSDate(task.startDateTime)) / 3600000
        taskToDisplay = <DraggableAndResizableComponent task={task} taskDuration={taskDuration} />

      }
      return taskStartDateTime
    })

  }

  searchTaskTimingMatchedWithGrid(props.tasks, props.date, props.hour)

  return (<>
    <div
      style={{
        width: '100px',
        height: '100px',
      }}
    >
      {taskToDisplay}
    </div>
  </>
  );
}

export default GridCell;
