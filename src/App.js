import React from 'react';
import './App.css';
import ButtonAppBar from './Header';
import WeeklyScheduleTemplateTable from './WeeklyScheduleTemplateTable';

function App() {
  return (<>
    <ButtonAppBar></ButtonAppBar>
    <WeeklyScheduleTemplateTable />
  </>
  );
}

export default App;
